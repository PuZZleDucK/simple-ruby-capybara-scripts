
require 'capybara'
require 'capybara/poltergeist'
require 'capybara/dsl'



session = Capybara::Session.new(:poltergeist)

session.visit "http://puzzleduck.org"

if session.has_content?("putting the DucKs")
  puts "Ducks in line"
else
  puts "The ducks have escaped!!!"
  exit(-1)
end
