#!/usr/bin/ruby

require 'capybara'
require 'capybara/poltergeist'
require 'capybara/dsl'
require 'cgi'
require 'timeout'
require 'find'



class GoogleWordGrabber
  include Capybara::DSL
  def initialize
    Capybara.default_driver = :poltergeist
  end

  def find_word_data(word)
    urls = []
    link = "https://www.google.com.au/#safe=off&q=#{CGI.escape(word)}"
    visit link
      page.all("h3.r a").each do |a|
#        puts "  - found: #{a.text[0..20]}... - #{a[:href]}"
        urls.push({"title" => a.text, "link" => a[:href].to_s})
      end
    return urls
  end
end




puts "Word search for #{ARGV[0]}"
#  if FileTest.directory?(path)
#      puts " -Directory: #{path}"
#    end
gwg = GoogleWordGrabber.new
word_links = gwg.find_word_data(ARGV[0])
#puts "word links: #{word_links}"

word_file = File::open("#{ARGV[0]}.txt","a")
word_links.each{ |w|
  puts "-writing: #{w["title"]}"
  word_file << "\"#{w["title"]}\" #{w["link"]}\n" # word_links
}










